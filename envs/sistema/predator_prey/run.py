from mesa.visualization.modules import CanvasGrid
from mesa.visualization.ModularVisualization import ModularServer
import torch

from predator import Predator
from prey import Prey

from model import TrainedPredatorPrey
from net import MLP

from torchreinforce import DeepReinforceModule

import config

device = torch.device("cpu")
action_size = 4
observation_size = (config.radius*2 + 1)**2
net = MLP(observation_size, action_size).to(device)

net.load_state_dict(torch.load("net.pth"))
net.eval()


def logic(observation):
    observation = torch.tensor(observation.flatten()).float()
    observation = observation.view(1, observation.numel())

    r_action = net(observation)
    action = r_action.get()
    if action == 0:
        return (1, 0)
    elif action == 1:
        return (-1, 0)
    elif action == 2:
        return (0, 1)
    elif action == 3:
        return (0, -1)


def draw(agent):
    if agent is None:
        return
    portrayal = {"Shape": "circle", "r": 1, "Filled": "true", "Layer": 0}
    if isinstance(agent, Predator):
        portrayal["Color"] = "Red"
    elif isinstance(agent, Prey):
        portrayal["Color"] = "Blue"
    return portrayal


grid = CanvasGrid(draw, config.map_size, config.map_size, 500, 500)
server = ModularServer(TrainedPredatorPrey,
                       [grid],
                       "Predator/Prey",
                       {"predators":config.predators, 
                        "preys":config.preys, 
                        "width":config.map_size, 
                        "height":config.map_size, 
                        "logic":logic, 
                        "freezepreys": config.freezepreys
                       }
                      )
    
server.port = 8521
server.launch()


from mesa import Agent, Model
from mesa.space import MultiGrid
from .predator import Predator
from .prey import Prey
import config
import random

from mesa.datacollection import DataCollector


def countPreys(model):
    return len(list(filter(lambda p: p.pos is not None, model.preys)))


def computeMeanReward(model):
    rewards = model.reward()
    mean_reward = sum(rewards)/len(rewards)
    if not hasattr(model, "mean_reward"): model.mean_reward = 0
    model.mean_reward += mean_reward
    return model.mean_reward


class PredatorPrey(Model):

    def __init__(self, predators, preys, width, height, freezepreys=False, **kwargs):
        super().__init__()
        self.seed = kwargs.get("seed", None)

        self.freezepreys = freezepreys
        self.ticks = 0

        if self.seed is not None: random.seed(self.seed)
        self.predators = []
        self.preys = []
        self.grid = MultiGrid(width, height, True)

        self.datacollector = DataCollector(
            model_reporters={"preys": countPreys, "mean_reward": computeMeanReward}
        )
        self.populate()

    def populate():
        for i in range(predators):
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            agent = Predator(i, self)
            self.predators.append(agent)
            self.grid.place_agent(agent, (x, y))
        
        for i in range(preys):
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            agent = Prey(predators + i, self)
            self.preys.append(agent)

            self.grid.place_agent(agent, (x, y))
    
    def step(self):
        self.ticks += 1
        self.datacollector.collect(self)

    def reward(self):
        rewards = []
        for predator in self.predators:
            rewards.append(predator.reward())
        return rewards


class TrainablePredatorPrey(PredatorPrey):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def observe(self):
        observations = []
        for predator in self.predators:
            observations.append(predator.observe())
        return observations

    def reset(self):
        self.reward()
        return self.observe()

    def step(self, directions):
        super().step()
        for predator, direction in zip(self.predators, directions):
            predator.step(direction)
        for prey in self.preys:
            if prey.pos is not None: prey.step()
        return self.observe(), self.reward(), self.is_done()
    
    def is_done(self):
        return self.ticks > config.max_ticks or len(list(filter(lambda p: p.pos is not None, self.preys))) == 0


class TrainedPredatorPrey(PredatorPrey):
    def __init__(self, logic, *args, **kwargs):
        self.logic = logic
        super().__init__(*args, **kwargs)
    
    def step(self):
        super().step()
        for predator in self.predators:
            observation = predator.observe()
            action = self.logic(observation)
            predator.step(action)
        
        for prey in self.preys:
            if prey.pos is not None: prey.step()


from .sistema.predator_prey.model import PredatorPrey
from mesa import Agent, Model
from .multiagentenv import MultiAgentEnv
import numpy as np
import math
from .sistema.predator_prey.predator import Predator
 

class PP_trainable(MultiAgentEnv, PredatorPrey):
    
    def __init__(self, predators, preys, radius, map_size, max_ticks):
        super().__init__(self, predators, preys, width=map_size, height=map_size, freezepreys=False, **kwargs)
        self.radius=radius
        self.episode_limit=max_ticks
        self.size=map_size
        self.n_agents=predator
        self.n_actions=8
        self._episode_steps=0
        
    def get_state_size():
        return (self.size*self.size)

    def get_state():
        obs = np.zeros((self.size, self.size))
        for predator in self.predators:
            obs[predator.pos[0]][predator.pos[1]] = -1
        
        for prey in self.preys:
            obs[prey.pos[0]][prey.pos[1]] = 1
            obs=np.append(obs.flatten())
        
        return obs
    
    def get_obs_size():
        return ((self.radius*2+1)**2)*self.n_agents
    
    def get_total_actions():
        return self.n_actions
    
    def get_obs():
        observations=[]
        for predator in self.predators:
            observations=observations.append(predator.observe().flatten())
        return observations
    
    def get_avail_actions():
        av_actions=[]
        for predator in self.predators:
            ag_aa=predator.av_act()
            av_actions=av_actions.append(ag_aa)
        return av_actions
         
    def step(self, actions):
           """A single environment step. Returns reward, terminated, info."""
           actions = [int(a) for a in actions]
           i=0
           for action in enumerate(actions):
                if(action==0):
                    self.predators[i].move((1,0))
                if(action==1):
                    self.predators[i].move((1,1))
                if(action==2):
                    self.predators[i].move((0,1))
                if(action==3):
                    self.predators[i].move((-1,1))
                if(action==4):
                    self.predators[i].move((-1,0))
                if(action==5):
                    self.predators[i].move((-1,-1))
                if(action==6):
                    self.predators[i].move((0,-1))
                if(action==7):
                    self.predators[i].move((1,-1))
           for prey in self.preys:
                prey.move()
           terminated=True
           for prey in self.preys:
                if(prey.life==1):
                    terminated=False
                
           self._episode_steps += 1
           reward=self.reward()
          

           if self._episode_steps >= self.episode_limit:
              
               terminated = True
              
           return reward, terminated, 0

    def reset():
        self.predators.clear()
        self.preys.clear()
        self.populate()
    
    

        
    
               
    
   

import unittest
from main import recursive_dict_update

class TestMain(unittest.TestCase):
    def test_recursive_dict_update_only_strings (self):
        first_dict = {'a':'b', 'c':{'g':'d'}}
        update_dict = {'a':'f'}
        exp_dict = {'a':'f', 'c':{'g':'d'}}
        act_dict = recursive_dict_update(first_dict, update_dict)
        self.assertEqual(exp_dict, act_dict)
    
    def test_recursive_dict_update_some_types (self):
        first_dict = {'a': 1, 'c':{'g': False}, 'foo' : 3}
        update_dict = {'a': 2, 'c':{'g': True}}
        exp_dict = {'a': 2, 'c':{'g': True}, 'foo': 3}
        act_dict = recursive_dict_update(first_dict, update_dict)
        self.assertEqual(exp_dict, act_dict)
    
    def test_recursive_dict_update_change_types (self):
        first_dict = {'a': 1, 'c':{'g': 'False'}, 'foo' : 3}
        update_dict = {'a': 2, 'c':{'g': True}}
        exp_dict = {'a': 2, 'c':{'g': True}, 'foo': 3}
        act_dict = recursive_dict_update(first_dict, update_dict)
        self.assertEqual(exp_dict, act_dict)
 

from enum import IntEnum


class Directions (IntEnum):
    NORTH = 0     # (0, 1)
    NORTHEAST = 1 # (1, 1)
    EAST = 2      # (1, 0)
    SOUTHEAST = 3 # (1, -1)
    SOUTH = 4     # (-1, 0)
    SOUTHWEST = 5 # (-1, -1)
    WEST = 6      # (-1, 0)
    NORTHWEST = 7 # (-1, 1)



dir2point = {    
    Directions.NORTH:       (0, 1),
    Directions.NORTHEAST:   (1, 1),
    Directions.EAST:        (1, 0),
    Directions.SOUTHEAST:   (1, -1),
    Directions.SOUTH:       (-1, 0),
    Directions.SOUTHWEST:   (-1, -1),
    Directions.WEST:        (-1, 0),
    Directions.NORTHWEST:   (-1, 1)
}




# COMA_MESA
This is a try to integrate the COMA algorithm from PyMARL (https://github.com/oxwhirl/pymarl/) with the MESA Simulator 
(https://mesa.readthedocs.io/en/master/overview.html). The code has been forked from a repository of giovanhero who is
a bachelor degree student of the University of Pisa.

# STATUS
IT IS A WIP, be careful in forking it, it is not tested enough for production.

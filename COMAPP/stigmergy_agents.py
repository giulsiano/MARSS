import numpy as np
import math
import itertools as it
import random
from mesa import Agent
from utils.direction import Directions, dir2point, point2dir
from enum import IntEnum, auto


class Wall (Agent):
    """
        MESA Agent to model walls inside the environment
    """
    def __init__ (self, unique_id, model, **kwargs):
        super().__init__(unique_id, model)
        self.color = kwargs.get("color", [0,0,0]) #TODO: use pygame/user defined colors
        self.width = kwargs.get("width", 1)
        self.height = kwargs.get("height", 1)

    def __hash__(self):
        return hash(Wall)

    def observe (self):
        """
            A wall can't observe things around it
        """
        return np.zeros(9, dtype=np.float32)

    def move (self):
        """
            Wall doesn't move
        """
        return
        
    def available_actions (self):
        return [0]

    def step (self):
        return


class Pheromone (Agent):
    """
        MESA Agent to model releasing of pheromones by drones into the environment
    """
    def __init__ (self, unique_id, model, **kwargs):
        super().__init__(unique_id, model)
        self.stig_value = kwargs.get("value", 10)
        self.evap_speed = kwargs.get("evaporation_speed", 1)
        self.stig_radius = kwargs.get("radius", 2)
        self.stig_evap_fun = kwargs.get("evaporation_fun", lambda v: v/2)
        self.color = kwargs.get("color", [255,0,0]) #TODO: use pygame/user defined colors
        self.width = kwargs.get("width", 2)
        self.height = kwargs.get("height", 2)

    def observe (self):
        """
           Pheromone agent doesn't observe. It decays.
        """
        return np.zeros(9, dtype=np.float32)

    def move (self):
        """
            Pheromone doesn't move
        """
        return
        
    def av_act (self):
        """
            The only available action for Pheromone is to decay
        """
        return [0]

    def evaporate (self):
        self.stig_value = self.stig_evap_fun(self.stig_value)

    def step (self):
        self.evaporate()
        if self.stig_value <= 0:
            self.model.remove_agent(self)


class Drone (Agent):
    """
        MESA Agent that simulates a drone
    """
    class Actions (IntEnum):
            MOVE_N = Directions.NORTH,
            MOVE_NE = Directions.NORTHEAST, 
            MOVE_E = Directions.EAST, 
            MOVE_SE = Directions.SOUTHEAST, 
            MOVE_S = Directions.SOUTH, 
            MOVE_SW = Directions.SOUTHWEST, 
            MOVE_W = Directions.WEST, 
            MOVE_NW = Directions.NORTHWEST, 
            NO_MOVE = Directions.NODIR,

            # Maximum movement value number is Directions.NORTHEAST
            RELEASE_PHEROMONE = Directions.NORTHEAST + 1

    run_action = {
            Actions.MOVE_N: lambda drone: drone.move(Directions.NORTH), 
            Actions.MOVE_NE: lambda drone: drone.move(Directions.NORTHEAST),
            Actions.MOVE_E: lambda drone: drone.move(Directions.EAST),
            Actions.MOVE_SE: lambda drone: drone.move(Directions.SOUTHEAST),
            Actions.MOVE_S: lambda drone: drone.move(Directions.SOUTH),
            Actions.MOVE_SW: lambda drone: drone.move(Directions.SOUTHWEST),
            Actions.MOVE_W: lambda drone: drone.move(Directions.WEST),
            Actions.MOVE_NW: lambda drone: drone.move(Directions.NORTHWEST),
            Actions.NO_MOVE: lambda drone: drone.move(Directions.NO_MOVE),

            # Maximum movement value number is Directions.NORTHEAST
            Actions.RELEASE_PHEROMONE: lambda drone: drone.release_pheromone()
            }

    def __init__ (self, unique_id, model, **kwargs):
        """
            kwargs:
                - color
                - obs_radius
                - logic
        """
        super().__init__(unique_id, model)
        self.color = kwargs.get("color", [255,255,255]) #TODO: use pygame/user defined colors
        self.obs_radius = kwargs.get("obs_radius", 2)
        self.logic = kwargs.get("logic", None)
        self.width = kwargs.get("width", 1)
        self.height = kwargs.get("height", 1)

    def observe (self):
        """
            Returns an observation made by the agent on its part of environment.
            Each cell of the observation is an array of 4 binary elements that specify what is 
            currently occupying the cell in this step:
            - [drone, wall, target]
        """
        # Get the cell around positions 
        neighborhood = self.model.grid.get_neighborhood(
            self.pos,
            moore = True,
            include_center = True,
            radius = self.obs_radius
            )
        available_movements = self.observe_available_movement(neighborhood, self.obs_radius)
        stigmergy_levels = self.observe_stigmergy_level(neighborhood, self.obs_radius)
        distance_to_targets = self.observe_distances_to_targets(neighborhood, self.obs_radius)
        distance_to_pheromones = self.observe_distances_to_pheromones(neighborhood, self.obs_radius)
        return np.concatenate((available_movements, 
                               stigmergy_levels,
                               distance_to_targets,
                               distance_to_pheromones
                               ))

    def _is_cell_empty (self, pos):
        return self.model.grid.is_cell_empty(pos)

    def _get_cell_contents (self, pos):
        return self.model.grid[pos[0]][pos[1]]

    def _adjust_around_neighborhood (self, neighborhood, radius):
        """
            Return a list of 9 cells that represent the full neighborhood. Mesa returns only
            neighbor of cells around an Agent considering corners and edges, this means it is returning 
            variable size lists (minimum 4, maximum 9 elements for around neighbors).
            This function will return the entire neighborhood at a distance radius from the agent

        """
        obs_size = 2 * radius + 1
        neigh_rel_pos = it.starmap(lambda x, y: (x - self.pos[0], y - self.pos[1]), neighborhood)
        neigh_pos = list(it.starmap(lambda x, y: (x + radius, y + radius), neigh_rel_pos))
        obs = np.empty((obs_size, obs_size), dtype=object)
        for (x, y), neigh_cell in zip(neigh_pos, neighborhood):
            # Observation matrix' coordinate system is transposed wrt the world grid
            obs[y][x] = self._get_cell_contents(neigh_cell)

        walls = lambda: set([Wall(0, self.model),])
        obs[obs == None] = walls()
        return list(obs.flatten())

    def observe_distances_to (self, agent_type, neighborhood, radius = 2):
        full_neighborhood = self._adjust_around_neighborhood(neighborhood, radius)
        
        dist_obs = np.ones(len(full_neighborhood), dtype = np.float32) * np.inf
        my_pos = np.array(self.pos)
        dist_fun = lambda a: np.linalg.norm(a - my_pos) # L2 norm (Euclidean distance) by default
        
        for idx, neighbors in enumerate(full_neighborhood):
            agents = list(filter(lambda n: isinstance(n, agent_type) , neighbors))
            for agent in agents:
                # TODO: make this different if multiple agents are allowed to stay on the same cell
                try:
                    dist_obs[idx] = dist_fun(np.array(agent.pos))
                except AttributeError:
                    # Fake walls added with _adjust_around_neighborhood() haven't any pos attribute
                    dist_obs[idx] = 1.0

        return list(dist_obs.flatten())

    def observe_distances_to_targets (self, neighborhood, radius = 2):
        return self.observe_distances_to(Target, neighborhood, radius)

    def observe_distances_to_pheromones (self, neighborhood, radius = 2):
        return self.observe_distances_to(Pheromone, neighborhood, radius)

    def observe_stigmergy_level (self, neighborhood, radius = 2):
        """
            Returns the np.ndarray of stigmergy level observation for this agent around itself
        """
        full_neighborhood = self._adjust_around_neighborhood(neighborhood, radius)
        stig_obs = np.zeros(len(full_neighborhood), dtype = np.float32)
        
        for idx, neighbors in enumerate(full_neighborhood):
            for neighbor in neighbors:
                stig_obs[idx] = neighbor.stig_value if isinstance(neighbor, Pheromone) else 0.0

        return stig_obs

    def observe_available_movement (self, neighborhood, radius = 2):
        """
            Returns the np.ndarray of available movements a Drone can do around itself
        """
        full_neighborhood = self._adjust_around_neighborhood(neighborhood, radius)
        move_obs = np.zeros(len(full_neighborhood), dtype = np.float32)

        for idx, neighbors in enumerate(full_neighborhood):
            move_obs[idx] = 0.0 if any(map(lambda n: isinstance(n, (Wall, Drone)), neighbors)) else 1.0

        # No move is ever possible
        my_pos_idx = math.ceil(len(full_neighborhood)/2) - 1
        move_obs[my_pos_idx] = 1.0
        return move_obs
    
    def get_agent_available_actions (self):
        """
            Return possible actions this agent can do
        """
        neighborhood = self.model.grid.get_neighborhood(
            self.pos,
            moore = True,
            include_center = True,
            radius = 1  # Only around itself
            )
        # A drone can move where it observe no presence of Walls and other Drones
        n_movement = len(Directions)
        av_act = np.zeros(len(Drone.Actions), dtype = np.float32)
        av_act[:n_movement] = self.observe_available_movement(neighborhood, radius = 1)

        # If the drone is over a target then it can release the pheromone
        targets = [isinstance(obj, Target) for obj in self._get_cell_contents(self.pos)]
        if any(targets):
            av_act[Drone.Actions.RELEASE_PHEROMONE.value] = 1.0
        
        return av_act

    def move (self, direction):
        """
            Use the model to move the agent to the right direction
        """
        offset = dir2point[direction]

        new_pos = self.pos[0] + offset[0], self.pos[1] + offset[1]
        self.model.move_agent(self, new_pos)

    def _step_MESA (self):
        chosen_action = random.choice(list(Drone.Actions)) if not self.logic else self.logic(self.observe())
        if chosen_action == self.Actions.RELEASE_PHEROMONE:
            self.release_pheromone()

        else:
            self.move(chosen_action)

        # TODO: this function should return nothing, I put a return here for testing purposes
        return chosen_action

    def _step_COMA (self, action):
        Drone.run_action[action](self)
        return None
        
    def step (self, *actions):
        """
            Possible actions for the Drone each episode. This part is chosen by the training network
        """
        # Drones can do only 1 action each step()
        if len(actions) > 0: return self._step_COMA(actions[0])
        else: return self._step_MESA()

    def release_pheromone (self):
        """
            Create the Pheromone agent on the position of the Drone
        """
        self.model.add_pheromones((self.pos,))


class Target (Agent):
    """
        MESA Agent to show the target of each agent
    """
    def __init__ (self, unique_id, model, **kwargs):
        super().__init__(unique_id, model)
        self.color = kwargs.get("color", [0,255,0]) #TODO: use pygame/user defined colors
        self.width = kwargs.get("width", 1)
        self.height = kwargs.get("height", 1)

    def observe (self):
        """
            Target doesn't observe
        """
        return [0]

    def move (self):
        """
            Target doesn't move
        """
        pass
        
    def av_act (self):
        """
            No action available for targets
        """
        return [0]

    def step (self):
        """
            A target do nothing during steps of training
        """
        pass


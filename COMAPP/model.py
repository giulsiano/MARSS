from mesa import Model
from mesa.space import MultiGrid
import config
import random

from mesa.datacollection import DataCollector
from learners import REGISTRY as le_REGISTRY
from runners import REGISTRY as r_REGISTRY
from controllers import REGISTRY as mac_REGISTRY
from components.episode_buffer import ReplayBuffer
from components.transforms import OneHot
from envs.PPEnv import PPEnv


def countPreys(model):
    return len(list(filter(lambda p: p.pos is not None, model.preys)))


def computeMeanReward (model):
    rewards = model.reward()
    mean_reward = sum(rewards)/len(rewards)
    if not hasattr(model, "mean_reward"): model.mean_reward = 0
    model.mean_reward += mean_reward
    return model.mean_reward


class PPComa (Model):
    def __init__(self, **kwargs):
        super().__init__()
        self.env = PPEnv(**kwargs)
        self.runner = EpisodeRunner(args = kwargs.get("args", None), logger = logger)

    def step (self):
        episode_batch = self.runner.run(test_mode = False)

    def reset (self):
        self.runner.reset()


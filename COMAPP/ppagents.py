import numpy as np
import math
import config
import os
import sys
from mesa import Agent
from utils.direction import Directions, dir2point, point2dir
from enum import IntEnum

def ag_obs (agent, radius):
    """ Return the observation made by the agent. Observation is a square of size = 2*radius + 1
    """
    # Function handler for readness
    is_empty = agent.model.grid.is_cell_empty
    get_cell_content = lambda t: agent.model.grid[t[0]][t[1]]

    # Get the around cell positions 
    neighborhood = agent.model.grid.get_neighborhood(
        agent.pos,
        moore = True,
        include_center = True,
        radius = radius
        )
    size = 2 * radius + 1
    obs = np.zeros((size, size), dtype=np.int8).flatten()

    for i, pos in enumerate(neighborhood):
        if not is_empty(pos):
            neighbors = get_cell_content(pos)
            for neighbor in neighbors:

                if isinstance(neighbor, Predator):
                    obs[i] = -1

                if isinstance(neighbor, Prey):
                    if (neighbor.life >= 1):
                        obs[i] = 1
                    else:
                        # The observation stores also the position of dead preys
                        obs[i] = -2
        
    return obs



class Predator(Agent):

    # TODO: this dict is for future intentions. If it works well with movement we can also add more 
    #       actions to preys and predators
    predator_actions = {
            "eat_n":  Directions.NORTHWEST,
            "eat_ne": Directions.NORTHEAST, 
            "eat_e":  Directions.EAST, 
            "eat_se": Directions.SOUTHEAST, 
            "eat_s":  Directions.SOUTH, 
            "eat_sw": Directions.SOUTHWEST, 
            "eat_w":  Directions.WEST, 
            "eat_nw": Directions.NORTHWEST, 
            }

    def __init__(self, unique_id, model, **kwargs):        
        self.kill = 0
        self.obs_radius = kwargs.get("obs_radius", 2)
        self.logic = kwargs.get("logic", None)
        super().__init__(unique_id, model)

    def observe (self):
        return ag_obs(self, self.obs_radius)

    def av_act (self):
        """ Return agent's available actions getting the neighborhood around self """
        # Initially all moves are allowed
        obs = ag_obs(self, 1)
        avail_moves = [cell_value == 0 for cell_value in obs]
        return avail_moves

    def move (self, direction):
        new_pos = self.model.grid.torus_adj(self.pos[0] + direction[0], self.pos[1] + direction[1])
        self.model.grid.move_agent(self, new_pos)
        neighbors = self.model.grid.get_neighbors(self.pos,
                                                     moore = True,
                                                     include_center = True,
                                                     radius = 1 
                                                     )
        # Kill the prey if the predator moves on it
        for neighbor in neighbors:
            if isinstance(neighbor, Prey) and self.pos == neighbor.pos: 
                self.kill += 1
                neighbor.life = 0
                self.model.grid.remove_agent(neighbor)

    def step (self, direction):
        self.move(direction)

    def reward (self):
        neighborhood = self.model.grid.get_neighbors(
            self.pos,
            moore=True,
            include_center=True,
            radius=self.obs_radius)
        
        dists = []
        for neighbor in neighborhood:
            if isinstance(neighbor, Prey):
                prey_torus_pos = self.model.grid.torus_adj(neighbor.pos)
                my_torus_pos = self.model.grif.torus_adj(self.pos)
                dists.append(math.sqrt((prey_torus_pos[0] - my_torus_pos[0])**2 + (prey_torus_pos[1] - my_torus_pos[1])**2))
        
        approach = 0
        mean_dist = 0
        if len(dists) != 0:
            mean_dist = sum(dists)/len(dists)
            if self.last_mean_dist is not None:
                if mean_dist < self.last_mean_dist:
                    approach = 1
            self.last_mean_dist = mean_dist       
        
        reward = config.kill_prize*self.kill + config.approach_prize*approach
        return reward*(config.max_ticks - self.model.ticks + 1)


class Prey (Agent):

    prey_actions = {
            "run_n":  Directions.NORTHWEST,
            "run_ne": Directions.NORTHEAST, 
            "run_e":  Directions.EAST, 
            "run_se": Directions.SOUTHEAST, 
            "run_s":  Directions.SOUTH, 
            "run_sw": Directions.SOUTHWEST, 
            "run_w":  Directions.WEST, 
            "run_nw": Directions.NORTHWEST, 
            }

    def __init__ (self, unique_id, model, **kwargs):
        super().__init__(unique_id, model)
        self.freezepreys = kwargs.get("freezepreys", False)
        self.logic = kwargs.get("logic", None)
        self.life = 1
        self.obs_radius = kwargs.get("obs_radius", 2)

    def observe (self):
        return ag_obs(self, self.obs_radius)

    def move (self):
        # TODO: add the neural network part for them to choose the direction
        step = [random.choice([-1,0,1]), random.choice([-1,0,1])]
        new_position = self.model.grid.torus_adj((self.pos[0] + step[0], self.pos[1] + step[1]))
        self.model.grid.move_agent(self, new_position)
        
    def av_act (self):
        # A prey can move everywhere if the cell is empty
        obs = ag_obs(self, 1)
        avail_moves = list((obs == 0).astype(int))
        avail_moves[Directions.NODIR] = 0
        return avail_moves

    def step (self):
        if not self.model.freezepreys: self.move()


import numpy as np
import unittest
import random
import math

from stigmergy_agents import Drone, Wall, Target, Pheromone
from envs import StigmergyEnv
from utils.direction import Directions


class AgentsTestCase (unittest.TestCase):
    def setUp (self):
        self.env = StigmergyEnv(width = 10, height = 10)

    def test_Drone_init (self):
        # Test defaults of Drone
        agent = Drone(1, self.env)
        self.assertEqual(agent.model, self.env)
        self.assertEqual(agent.unique_id, 1)
        self.assertEqual(agent.logic, None)
        self.assertEqual(agent.obs_radius, 2)
        
        # Test custom attributes for Drone
        agent = Drone(1, self.env, logic = 3, width = 2, height = 2, obs_radius = 3, color = [0,1,2])
        self.assertEqual(agent.model, self.env)
        self.assertEqual(agent.unique_id, 1)
        self.assertEqual(agent.logic, 3)
        self.assertEqual(agent.obs_radius, 3)
        self.assertEqual(agent.color, [0,1,2])
        self.assertEqual(agent.width, 2)
        self.assertEqual(agent.height, 2)

    def test_Drone_observe (self):
        agent = Drone(1, self.env, obs_radius=1)
        self.env.grid.place_agent(agent, (0,0))
        exp_obs = np.array([
                            0, 0, 0, # available movements
                            0, 1, 1,
                            0, 1, 1,
                            
                            0, 0, 0, # stigmergy observation
                            0, 0, 0,
                            0, 0, 0,
                            
                            np.inf, np.inf, np.inf, # distances to targets
                            np.inf, np.inf, np.inf,
                            np.inf, np.inf, np.inf,

                            np.inf, np.inf, np.inf, # distances to pheromones
                            np.inf, np.inf, np.inf,
                            np.inf, np.inf, np.inf,
                           ], dtype=np.float32)
        act_obs = agent.observe()
        self.assertTrue(np.array_equal(act_obs, exp_obs), f"{act_obs} != {exp_obs}")

        self.env.grid.remove_agent(agent)
        agent = Drone(1, self.env, obs_radius=2)
        self.env.grid.place_agent(agent, (0,0))
        exp_obs = np.array([0, 0, 0, 0, 0, # available movements
                            0, 0, 0, 0, 0,
                            0, 0, 1, 1, 1,
                            0, 0, 1, 1, 1,
                            0, 0, 1, 1, 1,

                            0, 0, 0, 0, 0, # stigmergy observation
                            0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0,

                            np.inf, np.inf, np.inf, np.inf, np.inf, # distances to targets 
                            np.inf, np.inf, np.inf, np.inf, np.inf,
                            np.inf, np.inf, np.inf, np.inf, np.inf,
                            np.inf, np.inf, np.inf, np.inf, np.inf,
                            np.inf, np.inf, np.inf, np.inf, np.inf,

                            np.inf, np.inf, np.inf, np.inf, np.inf, # distances to pheromones 
                            np.inf, np.inf, np.inf, np.inf, np.inf,
                            np.inf, np.inf, np.inf, np.inf, np.inf,
                            np.inf, np.inf, np.inf, np.inf, np.inf,
                            np.inf, np.inf, np.inf, np.inf, np.inf
                           ], dtype=np.float32)
        act_obs = agent.observe()
        self.assertTrue(np.array_equal(act_obs, exp_obs), f"{act_obs} != {exp_obs}")

    def test_Drone_observe_with_agents_around (self):
        radius = 2
        drone_pos = (4, 4)
        drone = Drone(1, self.env, obs_radius=radius)
        
        evap_value = 10.0
        agents = (
                # Put agent of type Drone on the 4 cell (top, bottom, left and right)
                ((drone_pos[0], drone_pos[1] + 1), Drone(2, self.env, obs_radius=radius)),
                ((drone_pos[0], drone_pos[1] - 1), Drone(3, self.env, obs_radius=radius)),
                ((drone_pos[0] - 1, drone_pos[1]), Drone(4, self.env, obs_radius=radius)),
                ((drone_pos[0] + 1, drone_pos[1]), Drone(5, self.env, obs_radius=radius)),
                
                # Put agent of type Target on top left and bottom right corners
                ((drone_pos[0] - 1, drone_pos[1] + 1), Target(6, self.env)),
                ((drone_pos[0] + 1, drone_pos[1] - 1), Target(7, self.env)),

                # Put agent of type Pheromone on top right and bottom left corners
                ((drone_pos[0] + 1, drone_pos[1] + 1), Pheromone(8, self.env)),
                ((drone_pos[0] - 1, drone_pos[1] - 1), Pheromone(9, self.env)),

                # Put agent of type Wall where there are Targets
                ((drone_pos[0] - 1, drone_pos[1] + 1), Wall(10, self.env)),
                ((drone_pos[0] + 1, drone_pos[1] - 1), Wall(11, self.env)),
                ) 
        
        for pos, agent in agents:
            self.env.grid.place_agent(agent, pos)

        exp_obs = np.array([
                    1.0, 1.0, 1.0, 1.0, 1.0, # Available movement obs
                    1.0, 1.0, 0.0, 0.0, 1.0,
                    1.0, 0.0, 1.0, 0.0, 1.0,
                    1.0, 0.0, 0.0, 1.0, 1.0,
                    1.0, 1.0, 1.0, 1.0, 1.0, 

                    0.0, 0.0, 0.0, 0.0, 0.0, # Stigmergy obs
                    0.0, 10.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 10.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,

                    np.inf, np.inf, np.inf, np.inf, np.inf, # Distances to targets
                    np.inf, np.inf, np.inf, 1.4142135, np.inf,
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    np.inf, 1.4142135, np.inf, np.inf, np.inf,
                    np.inf, np.inf, np.inf, np.inf, np.inf,

                    np.inf, np.inf, np.inf, np.inf, np.inf, # Distance to pheromones
                    np.inf, 1.4142135, np.inf, np.inf, np.inf,
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    np.inf, np.inf, np.inf, 1.4142135, np.inf,
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                ], dtype=np.float32)

        self.env.grid.place_agent(drone, drone_pos)
        act_obs = drone.observe()
        self.assertTrue(np.array_equal(act_obs, exp_obs), f"{act_obs} != {exp_obs}")

    def test_Drone_observe_available_movement_radius_1 (self):
        agent = Drone(1, self.env, obs_radius=1)
        agent_pos = (0, 0)
        self.env.grid.place_agent(agent, agent_pos)
        neig = self.env.grid.get_neighborhood(agent_pos, moore = True, include_center = True, radius = 1)
        exp_obs = np.array([0, 0, 0,
                            0, 1, 1,
                            0, 1, 1], dtype=np.float32)

        act_obs = agent.observe_available_movement(neig, 1)
        self.assertTrue(np.array_equal(act_obs, exp_obs), f"{act_obs} != {exp_obs}")
    
    def test_Drone__adjust_neighborhood_radius_1 (self):
        radius = 1
        agent_pos = ((0, 0), # Bottom left
                    (0, self.env.grid.height//2),   # Left edge
                    (self.env.grid.width//2, 0),    # Bottom edge
                    (self.env.grid.width//2, self.env.grid.height - 1), # Top edge
                    (self.env.grid.width - 1, self.env.grid.height//2), # Right edge
                )

        agent = Drone(1, self.env, obs_radius=radius)
        default_value = self.env.grid.default_val()

        exp_neig_bottom_left_corner = [
                    None, None,          None,
                    None, agent,         default_value,
                    None, default_value, default_value
               ]

        exp_neig_left_edge = [
                    None, default_value, default_value,
                    None, agent,         default_value,
                    None, default_value, default_value
               ] 

        exp_neig_bottom_edge = [
                    None,          None,          None,
                    default_value, agent,         default_value,
                    default_value, default_value, default_value
               ] 

        exp_neig_right_edge = [
                    default_value, default_value, None,
                    default_value, agent,         None,
                    default_value, default_value, None
               ] 

        exp_neig_top_edge = [
                    default_value, default_value, default_value,
                    default_value, agent,         default_value,
                    None,          None,          None
               ] 
        exp_neigs = [exp_neig_bottom_left_corner,
                     exp_neig_left_edge,
                     exp_neig_bottom_edge,
                     exp_neig_top_edge,
                     exp_neig_right_edge
                    ]
        for pos, exp_neig in zip(agent_pos, exp_neigs):
            self.env.grid.place_agent(agent, pos)
            neig = self.env.grid.get_neighborhood(pos, moore = True, include_center = True, radius = radius)
            act_neig = agent._adjust_around_neighborhood(neig, radius)
            self.assertEqual(len(act_neig), len(exp_neig))
            # Check if any object into actual neighborhood set elements is of the expected class
            self.assertTrue(all(map(lambda a, e: map(lambda o: isinstance(o, e), a), act_neig, exp_neig)))
            self.env.grid.remove_agent(agent)

    def test_Drone__adjust_neighborhood_radius_2 (self):
        radius = 2
        agent_pos = ((0, 0), # Bottom left
                    (0, self.env.grid.height//2),   # Left edge
                    (self.env.grid.width//2, 0),    # Bottom edge
                    (self.env.grid.width//2, self.env.grid.height - 1), # Top edge
                    (self.env.grid.width - 1, self.env.grid.height//2), # Right edge
                )

        agent = Drone(1, self.env, obs_radius=radius)
        default_value = self.env.grid.default_val()

        exp_neig_bottom_left_corner = [
                    None, None,  None,          None,          None,
                    None, None,  None,          None,          None,
                    None, None,  agent,         default_value, default_value,
                    None, None,  default_value, default_value, default_value,
                    None, None,  default_value, default_value, default_value
                    ] 

        exp_neig_left_edge = [
                    None, None, default_value, default_value, default_value,
                    None, None, default_value, default_value, default_value,
                    None, None, agent,         default_value, default_value,
                    None, None, default_value, default_value, default_value,
                    None, None, default_value, default_value, default_value
                ] 

        exp_neig_bottom_edge = [
                    None,          None,          None,          None,          None,    
                    None,          None,          None,          None,          None,    
                    default_value, default_value, agent,         default_value, default_value,
                    default_value, default_value, default_value, default_value, default_value,
                    default_value, default_value, default_value, default_value, default_value
               ] 

        exp_neig_right_edge = [
                    default_value, default_value, default_value, None, None,
                    default_value, default_value, default_value, None, None,
                    default_value, default_value, agent,         None, None,
                    default_value, default_value, default_value, None, None,
                    default_value, default_value, default_value, None, None
               ] 

        exp_neig_top_edge = [
                     default_value, default_value, default_value, default_value, default_value,
                     default_value, default_value, default_value, default_value, default_value,
                     default_value, default_value, agent,         default_value, default_value,
                     None,          None,          None,          None,          None,
                     None,          None,          None,          None,          None
               ] 
        exp_neigs = [exp_neig_bottom_left_corner,
                     exp_neig_left_edge,
                     exp_neig_bottom_edge,
                     exp_neig_top_edge,
                     exp_neig_right_edge
                    ]
        for pos, exp_neig in zip(agent_pos, exp_neigs):
            self.env.grid.place_agent(agent, pos)
            neig = self.env.grid.get_neighborhood(pos, moore = True, include_center = True, radius = radius)
            act_neig = agent._adjust_around_neighborhood(neig, radius)
            self.assertEqual(len(act_neig), len(exp_neig))
            # Check if any object into actual neighborhood set elements is of the expected class
            self.assertTrue(all(map(lambda a, e: map(lambda o: isinstance(o, e), a), act_neig, exp_neig)))
            self.env.grid.remove_agent(agent)
    
    def test_Drone_step_action_interface_for_COMA (self):
        drones = [Drone(1, self.env), Drone(1, self.env), Drone(1, self.env)]
        drone_pos = ((2,2), (4,4), (2,5))
        for drone, pos in zip(drones, drone_pos):
            self.env.grid.place_agent(drone, pos)
        
        # Check first group of actions
        drone_act = [Drone.Actions.MOVE_N, Drone.Actions.MOVE_N, Drone.Actions.MOVE_N]
        for drone, action in zip(drones, drone_act):
            drone.step(action)
        
        exp_drone_pos = [(2,3), (4,5), (2,6)]
        act_drone_pos = [drone.pos for drone in drones] 
        self.assertEqual(exp_drone_pos, act_drone_pos)

        # Check second group of actions
        drone_act = [Drone.Actions.MOVE_E, Drone.Actions.MOVE_E, Drone.Actions.MOVE_E]
        for drone, action in zip(drones, drone_act):
            drone.step(action)
        
        exp_drone_pos = [(3,3), (5,5), (3,6)]
        act_drone_pos = [drone.pos for drone in drones] 
        self.assertEqual(exp_drone_pos, act_drone_pos)

        # Check third group of actions
        drone_act = [Drone.Actions.RELEASE_PHEROMONE, 
                     Drone.Actions.RELEASE_PHEROMONE, 
                     Drone.Actions.RELEASE_PHEROMONE
                     ]
        for drone, action in zip(drones, drone_act):
            drone.step(action)
        
        grid_content = [self.env.grid[x][y] for (x, y) in exp_drone_pos]
        act_pheromone_pos = [agent.pos for agent in grid_content if isinstance(agent, Pheromone)] 
        self.assertEqual(exp_drone_pos, act_drone_pos)

    def test_Drone_observe_stigmergy_level (self):
        radius = 2
        drone_pos = (self.env.grid_size[0]//2, self.env.grid_size[1]//2)
        drone = Drone(1, self.env, obs_radius=radius)
        
        evap_value = 10.0
        agents = (
                # Put agent of type Drone on the 4 cell (top, bottom, left and right)
                ((drone_pos[0], drone_pos[1] + 1), Drone(2, self.env, obs_radius=radius)),
                ((drone_pos[0], drone_pos[1] - 1), Drone(3, self.env, obs_radius=radius)),
                ((drone_pos[0] - 1, drone_pos[1]), Drone(4, self.env, obs_radius=radius)),
                ((drone_pos[0] + 1, drone_pos[1]), Drone(5, self.env, obs_radius=radius)),
                
                # Put agent of type Target on top left and bottom right corners
                ((drone_pos[0] - 1, drone_pos[1] + 1), Target(6, self.env)),
                ((drone_pos[0] + 1, drone_pos[1] - 1), Target(7, self.env)),

                # Put agent of type Pheromone on top right and bottom left corners
                ((drone_pos[0] + 1, drone_pos[1] + 1), Pheromone(8, self.env)),
                ((drone_pos[0] - 1, drone_pos[1] - 1), Pheromone(9, self.env)),

                # Put agent of type Wall where there are Targets
                ((drone_pos[0] - 1, drone_pos[1] + 1), Wall(10, self.env)),
                ((drone_pos[0] + 1, drone_pos[1] - 1), Wall(11, self.env)),
                ) 
        
        for pos, agent in agents:
            self.env.grid.place_agent(agent, pos)

        exp_stigm_levels = np.array([
                    0.0, 0.0, 0.0, 0.0, 0.0, # Stigmergy obs
                    0.0, 10.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 10.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,
                ], dtype=np.float32)
        self.env.grid.place_agent(drone, drone_pos)
        neig = self.env.grid.get_neighborhood(drone_pos, moore = True, include_center = True, radius = radius)
        act_stigm_levels = drone.observe_stigmergy_level(neig)
        self.assertTrue(np.array_equal(act_stigm_levels, exp_stigm_levels), 
                                       f"{act_stigm_levels} != {exp_stigm_levels}")

    def test_Drone_get_agent_available_actions (self):
        radius = 2
        drone_pos = (self.env.grid_size[0]//2, self.env.grid_size[1]//2)
        drone = Drone(1, self.env, obs_radius=radius)
        agents = (
                # Put agent of type Drone on the 4 cell (top, bottom, left and right)
                ((drone_pos[0], drone_pos[1] + 1), Drone(2, self.env, obs_radius=radius)),
                ((drone_pos[0], drone_pos[1] - 1), Drone(3, self.env, obs_radius=radius)),
                ((drone_pos[0] - 1, drone_pos[1]), Drone(4, self.env, obs_radius=radius)),
                ((drone_pos[0] + 1, drone_pos[1]), Drone(5, self.env, obs_radius=radius)),
                
                # Put agent of type Target on top left and bottom right corners
                ((drone_pos[0] - 1, drone_pos[1] + 1), Target(6, self.env)),
                ((drone_pos[0] + 1, drone_pos[1] - 1), Target(7, self.env)),

                # Put agent of type Pheromone on top right and bottom left corners
                ((drone_pos[0] + 1, drone_pos[1] + 1), Pheromone(8, self.env)),
                ((drone_pos[0] - 1, drone_pos[1] - 1), Pheromone(9, self.env)),

                # Put agent of type Wall where there are Targets
                ((drone_pos[0] - 1, drone_pos[1] + 1), Wall(10, self.env)),
                ((drone_pos[0] + 1, drone_pos[1] - 1), Wall(11, self.env)),
                ) 
        
        for pos, agent in agents:
            self.env.grid.place_agent(agent, pos)

        exp_av_act = np.zeros(len(Drone.Actions), dtype = np.float32)
        exp_av_act[[Directions.NODIR,
                    Directions.SOUTHWEST,
                    Directions.NORTHEAST]] = 1.0

        self.env.grid.place_agent(drone, drone_pos)
        act_stigm_levels = drone.get_agent_available_actions()
        self.assertTrue(np.array_equal(act_stigm_levels, exp_av_act), 
                                       f"{act_stigm_levels} != {exp_av_act}")

        # Add a Pheromone on the position of the Drone and ask again which are the available actions
        exp_av_act[Drone.Actions.RELEASE_PHEROMONE.value] = 1.0
        self.env.grid.place_agent(Target(12, self.env), drone_pos)
        act_stigm_levels = drone.get_agent_available_actions()
        self.assertTrue(np.array_equal(act_stigm_levels, exp_av_act), 
                                       f"{act_stigm_levels} != {exp_av_act}")

    def test_Drone_observe_distances_to_radius__1 (self):
        radius = 1
        pos = (0, 5)
        drone = Drone(0, self.env)
        agents_pos = {
                Drone:[(0,4), (0,6), (1,5), (1,4)],
                Wall:[(2,3), (4,4), (1,1), (2,2)],
                Target:[(1,4), (1,2), (1,6), (0,5)],
                Pheromone:[(0,5), (0,3), (0,4), (1,4)]
                }
        # Put all the agents onto the grid
        self.env.grid.place_agent(drone, pos)
        for agent_type, positions in agents_pos.items():
            for position in positions:
                self.env.grid.place_agent(agent_type(random.random(), self.env), position)

        neigh = self.env.grid.get_neighborhood(pos, moore = True, include_center = True, radius = radius)
        exp_dist_obs = [np.inf, np.inf, 1.4142135,
                        np.inf, 0.0, np.inf,
                        np.inf, np.inf, 1.4142135
                        ]
        act_dist_obs = drone.observe_distances_to(Target, neigh, radius)
        self.assertEqual(act_dist_obs.count(np.inf), exp_dist_obs.count(np.inf))
        self.assertTrue(math.isclose(act_dist_obs[2], exp_dist_obs[2], rel_tol=1e-7))
        self.assertTrue(math.isclose(act_dist_obs[4], exp_dist_obs[4], rel_tol=1e-7))
        self.assertTrue(math.isclose(act_dist_obs[8], exp_dist_obs[8], rel_tol=1e-7))

        exp_dist_obs = [1.0, np.inf, np.inf,
                        1.0, np.inf, np.inf,
                        1.0, np.inf, np.inf
                        ]
        act_dist_obs = drone.observe_distances_to(Wall, neigh, radius)
        self.assertEqual(act_dist_obs.count(np.inf), exp_dist_obs.count(np.inf))
        self.assertTrue(math.isclose(act_dist_obs[0], exp_dist_obs[0], rel_tol=1e-7))
        self.assertTrue(math.isclose(act_dist_obs[3], exp_dist_obs[3], rel_tol=1e-7))
        self.assertTrue(math.isclose(act_dist_obs[6], exp_dist_obs[6], rel_tol=1e-7))

        exp_dist_obs = [np.inf, 1.0, 1.4142135,
                        np.inf, 0.0, np.inf,
                        np.inf, np.inf, np.inf
                       ]
        act_dist_obs = drone.observe_distances_to(Pheromone, neigh, radius)
        self.assertEqual(act_dist_obs.count(np.inf), exp_dist_obs.count(np.inf))
        self.assertTrue(math.isclose(act_dist_obs[8], exp_dist_obs[8], rel_tol=1e-7))
        self.assertTrue(math.isclose(act_dist_obs[7], exp_dist_obs[7], rel_tol=1e-7))
        self.assertTrue(math.isclose(act_dist_obs[4], exp_dist_obs[4], rel_tol=1e-7))

        exp_dist_obs = [np.inf, 1.0, 1.4142135,
                        np.inf, 0.0, 1.0,
                        np.inf, 1.0, np.inf,
                        ]
        act_dist_obs = drone.observe_distances_to(Drone, neigh, radius)
        self.assertEqual(act_dist_obs.count(np.inf), exp_dist_obs.count(np.inf))
        self.assertTrue(math.isclose(act_dist_obs[1], exp_dist_obs[1], rel_tol=1e-7))
        self.assertTrue(math.isclose(act_dist_obs[5], exp_dist_obs[5], rel_tol=1e-7))
        self.assertTrue(math.isclose(act_dist_obs[4], exp_dist_obs[4], rel_tol=1e-7))
        self.assertTrue(math.isclose(act_dist_obs[7], exp_dist_obs[7], rel_tol=1e-7))
        self.assertTrue(math.isclose(act_dist_obs[8], exp_dist_obs[8], rel_tol=1e-7))
        
    def test_Drone_step_interface_for_Mesa (self):
        drone = Drone(0, self.env)
        drone_pos = (4,4)
        chosen_actions = [False] * len(Drone.Actions)
        while (not all(chosen_actions)):
            self.env.grid.place_agent(drone, drone_pos) 
            act_action = drone.step()
            if act_action == Drone.Actions['MOVE_N']:
                self.assertEqual(drone.pos, (4,5))
                chosen_actions[Drone.Actions['MOVE_N']] = True

            elif act_action == Drone.Actions['MOVE_NE']:
                self.assertEqual(drone.pos, (5,5))
                chosen_actions[Drone.Actions['MOVE_NE']] = True

            elif act_action == Drone.Actions['MOVE_E']:
                self.assertEqual(drone.pos, (5,4))
                chosen_actions[Drone.Actions['MOVE_E']] = True

            elif act_action == Drone.Actions['MOVE_SE']:
                self.assertEqual(drone.pos, (5,3))
                chosen_actions[Drone.Actions['MOVE_SE']] = True

            elif act_action == Drone.Actions['MOVE_S']:
                self.assertEqual(drone.pos, (4,3))
                chosen_actions[Drone.Actions['MOVE_S']] = True

            elif act_action == Drone.Actions['MOVE_SW']:
                self.assertEqual(drone.pos, (3,3))
                chosen_actions[Drone.Actions['MOVE_SW']] = True

            elif act_action == Drone.Actions['MOVE_W']:
                self.assertEqual(drone.pos, (3,4))
                chosen_actions[Drone.Actions['MOVE_W']] = True

            elif act_action == Drone.Actions['MOVE_NW']:
                self.assertEqual(drone.pos, (3,5))
                chosen_actions[Drone.Actions['MOVE_NW']] = True

            elif act_action == Drone.Actions['NO_MOVE']:
                self.assertEqual(drone.pos, (4,4))
                chosen_actions[Drone.Actions['NO_MOVE']] = True
            
            else:
                agents = self.env.grid[drone_pos[0]][drone_pos[1]]
                self.assertTrue(any(map(lambda a: isinstance(a, Pheromone), agents)))
                chosen_actions[Drone.Actions['RELEASE_PHEROMONE']] = True
            self.env.grid.remove_agent(drone)


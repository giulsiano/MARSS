import numpy as np
import unittest

from agent import Prey, Predator
from envs import PPEnv
from utils.direction import Directions

class AgentTestCase (unittest.TestCase):

    def setUp (self):
        self.prey_val_in_obs = 1
        self.pred_val_in_obs = -1
        self.env = PPEnv(predators = 5, preys = 4, height = 5, width = 5)
        
        # Put predators
        self.env.put_agent((0, 4), Predator(0, self.env))
        self.env.put_agent((1, 3), Predator(1, self.env))
        self.env.put_agent((2, 2), Predator(2, self.env))
        self.env.put_agent((3, 1), Predator(3, self.env))
        self.env.put_agent((4, 0), Predator(4, self.env))

        # Put preys
        self.env.put_agent((0, 0), Prey(5, self.env))
        self.env.put_agent((1, 1), Prey(6, self.env))
        self.env.put_agent((3, 3), Prey(7, self.env))
        self.env.put_agent((4, 4), Prey(8, self.env))

    def test_ag_obs (self):
        agent_pos = (1,2)
        agent = Prey(9, self.env)
        self.env.put_agent(agent_pos, agent)
        exp_neighs = 9
        self.assertEqual(len(self.env.grid.get_neighbors(agent_pos, True, radius=2)), exp_neighs)
        exp_obs = np.array([-1, 1, 0, 0,  0,    # -1 = Predator, 1 = Prey
                             0, 0, 1, 0, -1,
                             0, 0, 1,-1,  0,
                             0, 0,-1, 0,  1,
                             1,-1, 0, 0,  0], dtype=np.int8)
        act_obs = agent.observe()
        self.assertTrue(np.array_equal(exp_obs, act_obs), f"\n{act_obs} !=\n{exp_obs}")
       
    def test_av_act (self):
        agent_pos = (3,4)
        agent = Prey(9, self.env)
        self.env.put_agent(agent_pos, agent)
        exp_neighs = 3
        neigh = self.env.grid.get_neighbors(agent.pos, True, radius = 1)
        self.assertEqual(len(self.env.grid.get_neighbors(agent.pos, True, radius = 1)), exp_neighs)
        exp_av_act = [1,0,1,1,0,0,1,1,0]
        act_av_act = agent.av_act()
        self.assertEqual(exp_av_act, act_av_act)

    def test_mesa_returns_ordered_coord (self):

        # check bottom left (origin)
        check_empty = self.env.grid.is_cell_empty
        neigh = self.env.grid.get_neighborhood((0, 0), True, include_center = True, radius = 1)
        exp_full_cells = [False,False,True,False,False,True,True,True,False]
        act_full_cells = [check_empty(cell) for cell in neigh]
        self.assertEqual(exp_full_cells, act_full_cells)
        
        # check centre
        neigh = self.env.grid.get_neighborhood((2, 2), True, include_center = True, radius = 1)
        exp_full_cells = [False,True,False,True,False,True,False,True,False]
        act_full_cells = [check_empty(cell) for cell in neigh]
        self.assertEqual(exp_full_cells, act_full_cells)

        # check top right
        neigh = self.env.grid.get_neighborhood((4, 4), True, include_center = True, radius = 1)
        exp_full_cells = [False,True,True,True,False,False,True,False,False]
        act_full_cells = [check_empty(cell) for cell in neigh]
        self.assertEqual(exp_full_cells, act_full_cells)

        # check top left
        neigh = self.env.grid.get_neighborhood((0, 4), True, include_center = True, radius = 1)
        exp_full_cells = [True,True,False,False,False,True,False,False,True]
        act_full_cells = [check_empty(cell) for cell in neigh]
        self.assertEqual(exp_full_cells, act_full_cells)

        # check bottom right
        neigh = self.env.grid.get_neighborhood((4, 0), True, include_center = True, radius = 1)
        exp_full_cells = [True,False,False,True,False,False,False,True,True]
        act_full_cells = [check_empty(cell) for cell in neigh]
        self.assertEqual(exp_full_cells, act_full_cells)

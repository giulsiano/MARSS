import unittest
import numpy as np
import random
from envs import StigmergyEnv
from stigmergy_agents import Drone, Pheromone, Wall, Target


class StigmergyEnvTestCase (unittest.TestCase):
    
    def setUp (self):
        random.seed(10)

    def test_StigmergyEnv_init (self):
        # Test default
        env = StigmergyEnv()
        self.assertEqual(env.grid.width, 10)
    
    def test_add_pheromones (self):
        env = StigmergyEnv()
        phero_pos = [(0,0), (4,4), (6,6)]
        env.add_pheromones(phero_pos)
        # 3 pheromones plus 3 around the one on (0,0), plus 8 around (4,4) plus 7 around the last one.
        # The last is only seven since there are a common cell in the neighborhood of the last two pheromones
        exp_pheromone_agents = 3 + 3 + 8 + 7
        act_pheromone_agents = len(env.agents[Pheromone])
        self.assertTrue(exp_pheromone_agents, act_pheromone_agents)
    
    def test_add_targets_walls_and_drones (self):
        env = StigmergyEnv()
        positions = [(0,0),(2,2),(3,3)]
        env.add_targets(positions)
        env.add_walls(positions)
        env.add_drones(positions)
        exp_drones = 3
        exp_walls = 3
        exp_targets = 3
        self.assertEqual(exp_drones, len(env.agents[Drone]))
        self.assertEqual(exp_targets, len(env.agents[Target]))
        self.assertEqual(exp_walls, len(env.agents[Wall]))

    def test_populate (self):
        env = StigmergyEnv()
        positions = [(0,0), (1,1), (2,2)]
        env.populate(Drone, positions)
        self.assertEqual(len(env.agents[Drone]), len(positions))
        self.assertTrue(all(map(lambda p, a: p[0] == a.pos[0] and p[1] == a.pos[1], positions, env.agents[Drone])))

    def test_remove_agent (self):
        env = StigmergyEnv()
        positions = [(0,0), (1,1), (2,2)]
        agents = [Drone(1, env) for pos in positions]
        for agent, pos in zip(agents, positions):
            env.grid.place_agent(agent, pos)
            env.agents[Drone].append(agent)

        for agent in agents:
            env.remove_agent(agent)

        self.assertTrue(all(map(lambda p: len(env.grid[p[0]][p[1]]) == 0, positions)))
        self.assertTrue(len(env.agents[Drone]) == 0)

    def test_get_obs (self):
        env = StigmergyEnv(map_width = 20, map_height = 20)
        positions = [(1,1), (4,4), (2,1)]
        env.populate(Drone, positions, obs_radius = 1)
        self.assertEqual(len(env.agents[Drone]), len(positions))
        exp_obs = [np.array([1, 1, 1, 
                             1, 1, 0, 
                             1, 1, 1, 

                             0, 0, 0, 
                             0, 0, 0, 
                             0, 0, 0,

                             np.inf, np.inf, np.inf, 
                             np.inf, np.inf, np.inf, 
                             np.inf, np.inf, np.inf,

                             np.inf, np.inf, np.inf, 
                             np.inf, np.inf, np.inf, 
                             np.inf, np.inf, np.inf
                            ], dtype=np.float32),

                   np.array([1, 1, 1,
                             1, 1, 1,
                             1, 1, 1,
                                     
                             0, 0, 0,
                             0, 0, 0,
                             0, 0, 0,

                             np.inf, np.inf, np.inf, 
                             np.inf, np.inf, np.inf, 
                             np.inf, np.inf, np.inf,

                             np.inf, np.inf, np.inf, 
                             np.inf, np.inf, np.inf, 
                             np.inf, np.inf, np.inf
                            ], dtype=np.float32),

                   np.array([1, 1, 1,
                             0, 1, 1,
                             1, 1, 1,
                                     
                             0, 0, 0,
                             0, 0, 0,
                             0, 0, 0,

                             np.inf, np.inf, np.inf, 
                             np.inf, np.inf, np.inf, 
                             np.inf, np.inf, np.inf,

                             np.inf, np.inf, np.inf, 
                             np.inf, np.inf, np.inf, 
                             np.inf, np.inf, np.inf
                            ], dtype=np.float32)]
        act_obs = env.get_obs()
        self.assertIsInstance(act_obs, list)
        self.assertTrue(all(map(lambda a, e: np.array_equal(a, e), act_obs, exp_obs)))

    def test_reset (self):
        env = StigmergyEnv(map_width = 20, map_height = 20)
        drone_pos = [(1,1), (4,4), (2,1)]
        wall_pos = [(0,0), (0,1), (0,2)]
        target_pos = [(2,2), (3,3), (10,10)]
        env.populate(Drone, drone_pos, obs_radius = 1)
        env.populate(Wall, wall_pos, obs_radius = 1)
        env.populate(Target, target_pos, obs_radius = 1)
        self.assertEqual(len(env.agents[Drone]), len(drone_pos))
        self.assertEqual(len(env.agents[Wall]), len(wall_pos))
        self.assertEqual(len(env.agents[Target]), len(target_pos))
        env.reset()
        all_agent_pos = [*drone_pos, *wall_pos, *target_pos]
        self.assertEqual(len(env.agents[Drone]), 0)
        self.assertEqual(len(env.agents[Wall]), 0)
        self.assertEqual(len(env.agents[Target]), 0)
        self.assertTrue(all(map(lambda p: len(env.grid[p[0]][p[1]]) == 0, all_agent_pos)))

    def test_get_obs_agent (self):
        env = StigmergyEnv(map_width = 10, map_height = 10)
        drone_pos = [(1,2),(3,3),(5,4)]
        wall_pos = [(0,0), (0,1), (0,2)]
        target_pos = [(2,2), (3,3), (9,9)]
        phero_pos = [(7,3), (7,4), (1,9)]
        env.populate(Wall, wall_pos, obs_radius = 1)
        env.populate(Target, target_pos, obs_radius = 1)
        env.populate(Pheromone, phero_pos, obs_radius = 1)
        test_agents = [Drone(0, env), Drone(1, env), Drone(2, env)]
        for agent, pos in zip(test_agents, drone_pos):
            env.grid.place_agent(agent, pos)
            env.agents[Drone].append(agent)

        exp_obs = np.array([
                    0.0, 0.0, 1.0, 1.0, 1.0, # Available movement obs
                    0.0, 0.0, 1.0, 1.0, 1.0,
                    0.0, 0.0, 1.0, 1.0, 1.0,
                    0.0, 1.0, 1.0, 1.0, 0.0,
                    0.0, 1.0, 1.0, 1.0, 1.0, 

                    0.0, 0.0, 0.0, 0.0, 0.0, # Stigmergy obs
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,

                    np.inf, np.inf, np.inf, np.inf, np.inf, # Distances to targets
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    np.inf, np.inf, np.inf, 1.0,    np.inf,
                    np.inf, np.inf, np.inf, np.inf, 2.2360679,
                    np.inf, np.inf, np.inf, np.inf, np.inf,

                    np.inf, np.inf, np.inf, np.inf, np.inf, # Distance to pheromones
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    ], dtype=np.float32)
        act_obs = env.get_obs_agent(0)
        self.assertTrue(np.array_equal(exp_obs, act_obs), f"{act_obs} {exp_obs}")
        exp_obs = np.array([
                    1.0, 1.0, 1.0, 1.0, 1.0, # Available movement obs
                    0.0, 1.0, 1.0, 1.0, 1.0,
                    1.0, 1.0, 1.0, 1.0, 1.0,
                    1.0, 1.0, 1.0, 1.0, 0.0,
                    1.0, 1.0, 1.0, 1.0, 1.0, 

                    0.0, 0.0, 0.0, 0.0, 0.0, # Stigmergy obs
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,

                    np.inf, np.inf,    np.inf, np.inf, np.inf, # Distances to targets
                    np.inf, 1.4142135, np.inf, np.inf, np.inf,
                    np.inf, np.inf,    0.0,    np.inf, np.inf,
                    np.inf, np.inf,    np.inf, np.inf, np.inf,
                    np.inf, np.inf,    np.inf, np.inf, np.inf,

                    np.inf, np.inf, np.inf, np.inf, np.inf, # Distance to pheromones
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    ], dtype=np.float32)
        act_obs = env.get_obs_agent(1)
        self.assertTrue(np.array_equal(exp_obs, act_obs))
        exp_obs = np.array([
                    1.0, 1.0, 1.0, 1.0, 1.0, # Available movement obs
                    0.0, 1.0, 1.0, 1.0, 1.0,
                    1.0, 1.0, 1.0, 1.0, 1.0,
                    1.0, 1.0, 1.0, 1.0, 1.0,
                    1.0, 1.0, 1.0, 1.0, 1.0, 

                    0.0, 0.0, 0.0, 0.0, 0.0, # Stigmergy obs
                    0.0, 0.0, 0.0, 0.0, 10.0,
                    0.0, 0.0, 0.0, 0.0, 10.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,

                    np.inf,     np.inf, np.inf, np.inf, np.inf, # Distances to targets
                    2.2360679,  np.inf, np.inf, np.inf, np.inf,
                    np.inf,     np.inf, np.inf, np.inf, np.inf,
                    np.inf,     np.inf, np.inf, np.inf, np.inf,
                    np.inf,     np.inf, np.inf, np.inf, np.inf,

                    np.inf, np.inf, np.inf, np.inf, np.inf, # Distance to pheromones
                    np.inf, np.inf, np.inf, np.inf, 2.2360679,
                    np.inf, np.inf, np.inf, np.inf, 2.0,
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    np.inf, np.inf, np.inf, np.inf, np.inf,
                    ], dtype=np.float32)
        act_obs = env.get_obs_agent(2)
        self.assertTrue(np.array_equal(exp_obs, act_obs))
       
    def test_get_env_info (self):
        pass

    def test_get_state_size (self):
        env = StigmergyEnv(map_width=10, map_height=10)
        drones = [Drone(0, env), Drone(1, env), Drone(2, env)]
        positions = [(1,1), (3,3), (5,5)]
        for drone, pos in zip(drones, positions):
            env.grid.place_agent(drone, pos)
            env.agents[Drone].append(drone)
        
        # state size = observation size times number of drones
        exp_state_size = ((2*2 + 1)**2)*4 * len(drones)
        act_state_size = env.get_state_size()
        self.assertEqual(exp_state_size, act_state_size)

    def test_get_obs_size (self):
        env = StigmergyEnv(map_width=10, map_height=10)
        drone = Drone(0, env, obs_radius=1)
        env.grid.place_agent(drone, (1,1))
        env.agents[Drone].append(drone)
        # a square having 2*radius + 1 side length times number of possible observations
        exp_obs_size = ((2*1+1)**2)*4    
        act_obs_size = env.get_obs_size()
        self.assertEqual(act_obs_size, exp_obs_size)
        env.grid.remove_agent(drone)
        env.agents[Drone].remove(drone)

        drone = Drone(0, env, radius=2)
        env.grid.place_agent(drone, (1,1))
        env.agents[Drone].append(drone)
        exp_obs_size = ((2*2+1)**2)*4
        act_obs_size = env.get_obs_size()
        self.assertEqual(act_obs_size, exp_obs_size)

        env.grid.remove_agent(drone)
        env.agents[Drone].remove(drone)
        self.assertRaises(ValueError, env.get_obs_size)

    def test_get_total_actions (self):
        env = StigmergyEnv()
        exp_num_action = 10 # Movement around + no move + release pheromone
        act_num_action = env.get_total_actions()
        self.assertEqual(exp_num_action, act_num_action)

    def test_get_n_agents (self):
        env = StigmergyEnv(map_width = 15, map_height = 15)
        drone_pos = [(1,2), (3,4), (5,6)]
        wall_pos = [(0,0), (0,1), (0,2)]
        target_pos = [(2,2), (3,3), (10,10)]
        phero_pos = [(6,6), (7,6), (5,6)]
        env.populate(Drone, drone_pos, obs_radius = 1)
        env.populate(Wall, wall_pos, obs_radius = 1)
        env.populate(Target, target_pos, obs_radius = 1)
        env.populate(Pheromone, phero_pos)
        exp_n_agents = len(drone_pos)
        act_n_agents = env.get_n_agents()
        self.assertEqual(exp_n_agents, act_n_agents)

    def test_build_wall (self):
        env = StigmergyEnv()
        wall_start_pos = (0,0)
        wall_end_pos = (0,2)
        walls_len = 3
        wall_orientation = "vertical"
        exp_wall_pos = [(0,0), (0,1), (0,2)]
        act_wall_pos = env.build_wall(wall_start_pos, wall_end_pos, wall_orientation)
        self.assertEqual(exp_wall_pos, act_wall_pos)

        env = StigmergyEnv()
        wall_start_pos = (2,0)
        wall_end_pos = (5,0)
        walls_len = 4
        wall_orientation = "horizontal"
        exp_wall_pos = [(2,0), (3,0), (4,0), (5,0)]
        act_wall_pos = env.build_wall(wall_start_pos, wall_end_pos, wall_orientation)
        self.assertEqual(exp_wall_pos, act_wall_pos)

        env = StigmergyEnv()
        wall_start_pos = (2,2)
        wall_end_pos = (5,5)
        walls_len = 4
        wall_orientation = "diagonal"
        exp_wall_pos = [(2,0), (3,0), (4,0), (5,0)]
        with self.assertRaises(ValueError):
            act_wall_pos = env.build_wall(wall_start_pos, wall_end_pos, wall_orientation)
        

import unittest

from envs import PPEnv
from agent import Prey


class PPEnvTestCase (unittest.TestCase):
    
    def test_put_agent_inside_grid (self):
        env = PPEnv()
        exp_positions = ((1,1), (2,2), (3,3))
        i = 0
        for pos in exp_positions:
            env.put_agent(pos, Prey(i, self))
            i += 1

        for pos in exp_positions:
            self.assertFalse(env.grid.is_cell_empty(pos))

    def test_find_empty_position (self):
        # Default configuration
        env = PPEnv()
        pos = env._find_empty_position()
        self.assertIsInstance(pos, tuple)

    def test_get_obs (self):
        env = PPEnv(preys=5, predators=1, radius=2)
        env.reset()
        act_obs = env.get_obs()

        # Create a list enough large: n_agents * obs_size
        exp_obs_shape = ((5 + 1), (2*2 + 1)**2)
        self.assertEqual(exp_obs_shape, act_obs.shape)

    def test_reset (self):
        env = PPEnv(predators = 3, preys = 4)
        env.reset()
        self.assertEqual(len(env.predators), 3)
        self.assertEqual(len(env.preys), 4)
        self.assertEqual(env.size, 100)


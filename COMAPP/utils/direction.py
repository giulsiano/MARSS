from enum import IntEnum


class Directions (IntEnum):
    """
    +--------+-----------+--------+
    | NW = 6 | N     = 7 | NE = 8 |
    | W  = 3 | NODIR = 4 | E  = 5 |
    | SW = 0 | S     = 1 | SE = 2 |
    +--------+-----------+--------+
    """
    NORTH = 7     # (0, 1)
    NORTHEAST = 8 # (1, 1)
    EAST = 5      # (1, 0)
    SOUTHEAST = 2 # (1, -1)
    SOUTH = 1     # (-1, 0)
    SOUTHWEST = 0 # (-1, -1)
    WEST = 3      # (-1, 0)
    NORTHWEST = 6 # (-1, 1)
    NODIR = 4



dir2point = {    
    Directions.NORTH:       (0, 1),
    Directions.NORTHEAST:   (1, 1),
    Directions.EAST:        (1, 0),
    Directions.SOUTHEAST:   (1, -1),
    Directions.SOUTH:       (0, -1),
    Directions.SOUTHWEST:   (-1, -1),
    Directions.WEST:        (-1, 0),
    Directions.NORTHWEST:   (-1, 1),
    Directions.NODIR:       (0, 0)
}


point2dir = {    
    (0, 1):     Directions.NORTH,
    (1, 1):     Directions.NORTHEAST,
    (1, 0):     Directions.EAST,
    (1, -1):    Directions.SOUTHEAST,
    (0, -1):    Directions.SOUTH,
    (-1, -1):   Directions.SOUTHWEST,
    (-1, 0):    Directions.WEST,
    (-1, 1):    Directions.NORTHWEST,
    (0, 0):     Directions.NODIR
}



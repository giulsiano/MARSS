

def get_toroidal_around(pos, center, radius):
    start_torus = (center[0] - radius, center[1] - radius )
    torus_size = radius*2 + 1
    return [(pos[0] - start_torus[0])%torus_size, (pos[1] - start_torus[1])%torus_size]

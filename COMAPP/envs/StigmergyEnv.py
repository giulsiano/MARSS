from mesa import Agent, Model
from mesa.space import MultiGrid
from .multiagentenv import MultiAgentEnv
import numpy as np
import math
import random
from utils.direction import Directions, dir2point
from stigmergy_agents import Drone, Pheromone, Wall, Target


class StigmergyEnv(MultiAgentEnv):
    """
        This class provides a way to manage a stigmery enabled environment for using it with COMA
        and other algorithms from pymarl
    """

    def __init__(self, **kwargs):
        self.stig_value = kwargs.get("stigmergy_value", 10)
        self.stig_speed = kwargs.get("evaporation_speed", 1)
        self.stig_radius = kwargs.get("stigmergy_radius", self.stig_value//self.stig_speed + 1)
        self.evap_speed = kwargs.get("evaporation_speed", 1)
        self.episode_limit = kwargs.get("episode_limit", 10000)
        self.grid_size = (kwargs.get("map_width", 10),   # x size
                           kwargs.get("map_height", 10)    # y size
                         )

        self.drones_conf = kwargs.get("drones", None) 
        self.targets_conf = kwargs.get("targets", None) 
        self.pheromones_conf = kwargs.get("pheromones", None) 
        self.walls_conf = kwargs.get("walls", None) 

        self.grid = MultiGrid(self.grid_size[0], self.grid_size[1], False)  # no Torus
        self.episode_steps = 0
        self.agents = {
            Drone: [],
            Target: [],
            Pheromone: [],
            Wall: [],
            }
    
    def remove_agent (self, agent):
        agent_class = type(agent)
        self.agents[agent_class].remove(agent)
        self.grid.remove_agent(agent)

    def add_pheromones (self, pos):
        """
            Add a pheromone agent to the world
        """
        phero_params = {
                "stigmergy_value": 10.0,
                "stigmergy_radius": 1,
                "stigmergy_evaporation_fun": lambda v: v - self.evap_speed,
                "color": [0,255,0], 
                "width": 1, 
                "height": 1
                }

        # Put Pheromone agents all around each agent (each Pheromone agent produces another phero
        # agent and puts it around the former
        self.populate(Pheromone, pos, **phero_params)

        # Use a set here to have unique coordinates (positions) to put the Pheromone agents required
        # TODO: verify with Federico about this part. In this implementation there are not allowed to put
        # different pheros on the grid and it is also not allowed to have more than one Pheromone agent
        # in a cell
        spreaded_pheros = list(map(lambda p: self.grid.get_neighborhood(p, 
                                                                        moore=True, 
                                                                        include_center=False,
                                                                        radius=phero_params["stigmergy_radius"]
                                                                        ), pos))
        for neighbors in spreaded_pheros:
            self.populate(Pheromone, neighbors, **phero_params)
    
    def add_targets (self, pos):
        """
            Put agents of type Target into the world.
            args = [(pos_x_target_1, pos_y_target_1), (pos_x_target_2, pos_y_target_2), ...]
        """
        #TODO: make this initialization parameter dictionary user definable
        target_params = {
                "color": [0,255,0], 
                "width": 1, 
                "height": 1
                }
        self.populate(Target, pos, **target_params)
    
    def add_drones (self, pos):
        """
            Put agents of type Drone into the world.
            args = [(pos_x_drone_1, pos_y_drone_1), (pos_x_drone_2, pos_y_drone_2), ...]
        """
        #TODO: make this initialization parameter dictionary user definable
        drone_params = {
                "color": [255,255,255],#TODO: use pygame/user defined colors
                "obs_radius": 2,
                "logic": None,
                "width": 1,
                "height": 1,
                }
        self.populate(Drone, pos, **drone_params)

    def add_walls (self, pos):
        """
            Put agents of type Wall into the world.
            args = [(pos_x_wall_1, pos_y_wall_1), (pos_x_wall_2, pos_y_wall_2, ...]

        """
        #TODO: make this initialization parameter dictionary user definable
        wall_params = {
                "color": [0,0,0],#TODO: use pygame/user defined colors
                "width": 1,
                "height": 1,
                }
        self.populate(Wall, pos, **wall_params)

    def populate (self, agent_type, agent_pos, **agent_kwargs):
        """
            Put the number of agent requested by the list of positions passed via agent_pos, 
            each agent is initialized by using agent_kwargs
        """
        # Put all the requested agent type on the world. But first build their unique_id attribute
        uids = map(lambda pos: hash(random.random() + pos[0] + pos[1]), agent_pos)
        agent_list = list(map(lambda uid: agent_type(uid, self, **agent_kwargs), uids))
        for pos, agent in zip(agent_pos, agent_list):
            self.grid.place_agent(agent, pos)

        self.agents[agent_type].extend(agent_list)
    
    def build_wall (self, start, end, direction):
        s_x, s_y = start
        e_x, e_y = end

        if direction == "vertical":
            if s_x != e_x: raise ValueError(f"Vertical wall should have x coordinate constant")
            range_start = min((s_y, e_y))
            range_end = max((s_y, e_y))
            return [(s_x, wall_y) for wall_y in range(range_start, range_end + 1)]

        elif direction == "horizontal":
            if s_y != e_y: raise ValueError(f"Horizontal wall should have y coordinate constant")
            range_start = min((s_x, e_x))
            range_end = max((s_x, e_x))
            return [(wall_x, s_y) for wall_x in range(range_start, range_end + 1)]

        else:
            raise ValueError(f"direction can be horizontal or vertical. Not {direction}")
    
    def get_state_size (self):
        return self.get_obs_size() * len(self.agents[Drone])

    def get_state (self):
        # TODO: it returns only the observations of agents so far. Maybe the observation has also other
        # features to take in account. Code has been taken from SMAC
        return np.concatenate(self.get_obs(), axis = 0).astype(np.float32)
    
    def get_obs_size (self):
        try:
            return self.agents[Drone][0].observe().size

        except IndexError:
            raise ValueError("No Drone can observe the environment so far")
    
    def get_total_actions (self):
        return len(Drone.Actions)
    
    def get_obs (self):
        # Return observations as a list for each agent
        return [agent.observe() for agent in self.agents[Drone]]
    
    def get_avail_actions (self):
        """
            Returns all the available actions for all agents
        """
        return [agent.get_avail_agent_actions() for agent in self.agents[Drone]]

    def step (self, actions):
        """A single environment step. Returns reward, terminated, info."""
        for action, agent in zip(actions, self.agents[Drone]):
            agent.step(actions)

        # Consider this episode terminated when all the agents have done their job
        self.episode_steps += 1
        terminated = self.episode_steps >= self.episode_limit
        reward = self.reward()

        return reward, terminated, self.get_env_info()

    def reset (self):
        # Clear the board and the lists of agents
        for agent_list in self.agents.values():
            for agent in agent_list:
                self.grid.remove_agent(agent)
            agent_list.clear()
        self.episode_steps = 0
    
    def reward (self):
        """ Reward in COMA are centralized """
        #TODO: define a reward for each agent to get at each step
        return 1

    def get_obs_agent (self, agent_id):
        """ Returns observation for agent_id """
        agent = list(filter(lambda a: a.unique_id == agent_id, self.agents[Drone]))

        if len(agent) > 1: raise ValueError(f"agent with same unique_id has been found: {agent}")
        else: return agent[0].observe()

    def get_n_agents (self):
        return len(self.agents[Drone])

    def render (self):
        raise NotImplementedError

    def close (self):
        raise NotImplementedError

    def seed (self):
        raise NotImplementedError

    def save_replay (self):
        raise NotImplementedError

    def get_env_info (self):
        env_info = {"state_shape": self.get_state_size(),
                    "obs_shape": self.get_obs_size(),
                    "n_actions": self.get_total_actions(),
                    "n_agents": self.get_n_agents(),
                    "episode_limit": self.episode_limit <= self.episode_steps}
        return env_info

    def move_agent (self, agent, position):
        self.grid.move_agent(agent, position)

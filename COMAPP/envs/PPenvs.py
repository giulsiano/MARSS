import numpy as np
import math
import random
import logging
from utils.direction import Directions, dir2point
from mesa.space import MultiGrid
from mesa import Agent, Model
from .multiagentenv import MultiAgentEnv
from ppagents import Predator, Prey


class PPEnv(MultiAgentEnv):
    
    def __init__ (self, **kwargs): # predators, preys, obs_radius, map_size, max_episodes):
        self.radius = kwargs.get("radius", 2)   # 2 cell on each side of the agent
        self.episode_limit = kwargs.get("episode_limit", 10000)
        self.grid_size = [ kwargs.get("width", 10),   # x size
                           kwargs.get("height", 10)     # y size
                         ]
        self.size = self.grid_size[0] * self.grid_size[1]
        self.grid = MultiGrid(self.grid_size[0], self.grid_size[1], True)  # Torus/pacman effect enabled
        self.n_preys = kwargs.get("preys", 10)
        self.n_predators = kwargs.get("predators", 10)
        self.n_agents = self.n_preys + self.n_predators
        self.n_actions = len(actions)
        self.predators = []
        self.preys = []

    def _find_empty_position (self):
        if self.grid.exists_empty_cells():
            x = random.randrange(self.grid.width)
            y = random.randrange(self.grid.height)
            while not self.grid.is_cell_empty((x, y)):
                x = random.randrange(self.grid.width)
                y = random.randrange(self.grid.height)

            return (x, y)
 
    def _populate (self, preys, predators):
        self.n_agents = preys + predators
        self.n_preys = preys
        self.n_predators = predators
        for i in range(self.n_predators):
            pos = self._find_empty_position()
            agent = Predator(i, self)
            self.predators.append(agent)
            self.grid.place_agent(agent, pos)
        
        for i in range(self.n_preys):
            pos = self._find_empty_position()
            agent = Prey(self.n_predators + i, self)
            self.preys.append(agent)
            self.grid.place_agent(agent, pos)
       
    def get_state_size (self):
        return self.size

    def get_state (self):
        obs = np.zeros((self.grid_size[0], self.grid_size[1]))
        for predator in self.predators:
            obs[predator.pos[0]][predator.pos[1]] = -1

        for prey in self.preys:
            if prey.pos is not None:
                obs[prey.pos[0]][prey.pos[1]] = 1
        
        return obs.flatten()
    
    def get_obs_size (self):
        return ((2*self.radius + 1)**2) * self.n_agents
    
    def get_total_actions (self):
        return self.n_actions
    
    def get_obs (self):
        observations = []
        for predator in self.predators:
            observations.append(predator.observe())

        for prey in self.preys:
            observations.append(prey.observe())
        
        return np.array(observations)
    
    def get_avail_actions (self):
        """Returns the available actions of all agents in a list."""
        av_actions = []
        agents = [*self.predators, *self.preys]
        return [agent.av_act() for agent in agents]

    def get_avail_agent_actions (self, agent_id):
        """ Returns the available actions for agent_id """
        # Agent_id is incremental, first there are id from 0 to predators-1 for 
        # recognizing predators, the rest of agents are preys
        agents = preys if agent_id >= self.n_predators else predators
        for agent in agents:
            if agent.unique_id == agent_id: break
        return agent.av_act()

    def step (self, actions):
        """A single environment step. Returns reward, terminated, info."""
        for action in actions:
            toPoint = dir2Point[Directions(action)]
            self.predators[i].move(toPoint)
                   
        for prey in self.preys:
            prey.move()

        terminated = all([prey.life <= 0 for prey in preys])
        reward = self.reward()

        return reward, terminated, 0

    def reset (self):
        self.predators.clear()
        self.preys.clear()
        self._populate(self.n_preys, self.n_predators)
    
    def put_agent (self, pos, agent):
        self.grid.place_agent(agent, pos)

    def reward (self):
        """ Reward in COMA are centralized """
        # TODO: these call to get neighbors is useless since the reward for COMA are centralized
        neighborhood = self.grid.get_neighbors(
                       self.pos, 
                       moore=True,
                       include_center=True,
                       radius=self.radius)
        
        dists = []
        # TODO: make the reward having a sense here. For each predator, for each prey
        for neighbor in neighborhood:
            if isinstance(neighbor, Prey):
                prey_torus_pos = get_toroidal_around(neighbor.pos, self.pos, self.radius)
                prey_torus_pos = get_toroidal_around(self.pos, self.pos, self.radius)
                dists.append(math.sqrt((prey_torus_pos[0] - my_torus_pos[0])**2 + (prey_torus_pos[1] - my_torus_pos[1])**2))
        
        approach = 0
        mean_dist = 0
        if len(dists) != 0:
            mean_dist = sum(dists)/len(dists)
            if self.last_mean_dist is not None:
                if mean_dist < self.last_mean_dist:
                    approach = 1
            self.last_mean_dist = mean_dist       
        
        reward = config.kill_prize * self.kill + config.approach_prize * approach
        return reward * (self.model.max_ticks - self.model.ticks + 1)


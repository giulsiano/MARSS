from functools import partial
from .multiagentenv import MultiAgentEnv
from .PPenvs import PPEnv
from .StigmergyEnv import StigmergyEnv
import sys
import os

def env_fn(env, **kwargs) -> MultiAgentEnv:
    return env(**kwargs)

REGISTRY = {}
REGISTRY["PP"] = partial(env_fn, env=PPEnv)
REGISTRY["Stigmergy"] = partial(env_fn, env=StigmergyEnv) 
if sys.platform == "linux":
    os.environ.setdefault("SC2PATH",
                          os.path.join(os.getcwd(), "3rdparty", "StarCraftII"))

import yaml
import time
import argparse
import sys
import logging
import collections
import torch as th
from types import SimpleNamespace as SN
from learners import REGISTRY as le_REGISTRY
from controllers import REGISTRY as mac_REGISTRY
from runners import REGISTRY as r_REGISTRY
from components.episode_buffer import ReplayBuffer
from components.transforms import OneHot

_DESCRIPTION = "Train and run a MARL system using COMA"
_LOG_FILE = "COMESA.log"
_LOGGER_NAME = "ComesaLogger"
_LOG_FORMAT = '%(asctime)s,%(msecs)d %(funcName)s %(levelname)s %(message)s'


def read_conf(path):
    config_dict = None
    with open(path, "r") as f:
       try:
           config_dict = yaml.full_load(f)

       except yaml.YAMLError as exc:
           logging.getLogger(_LOGGER_NAME).error("{}.yaml error: {}".format(config_name, exc))
           sys.exit(1)

       return config_dict


def recursive_dict_update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.Mapping):
            d[k] = recursive_dict_update(d.get(k, {}), v)
        else:
            d[k] = v
    return d


# Initialize data structures using yaml config file
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(prog = "COMESA", description = _DESCRIPTION)
    parser.add_argument("--train", help="Run the program only for training")
    parser.add_argument("-f", "--model-file", 
                        help="Where to store the model after training",
                        default="trained.th"
                        )
    parser.add_argument("-ce", "--config-env", 
                        help="Yaml configuration file of the env to run",
                        default="config/envs/Stigmergy.yaml"
                        )
    parser.add_argument("-ca", "--config-alg",
                        help="Path to yaml algorithm configuration file",
                        default="config/algs/coma_smac.yaml"
                        )
    args = parser.parse_args()

    # Init logger
    logging.basicConfig(filename=_LOG_FILE,
                       filemode='w',
                       format=_LOG_FORMAT,
                       datefmt='%H:%M:%S',
                       level=logging.DEBUG
                       )
    logger = logging.getLogger(_LOGGER_NAME)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logging.Formatter(_LOG_FORMAT))
    logger.addHandler(consoleHandler)
    
    # Read the default configuration
    logger.info("Reading default.yaml file")
    config = read_conf("config/default.yaml")
 
    # Load algorithm and environment base configs 
    logger.info("Reading algorithm and environment configurations")
    env_config = read_conf(args.config_env)
    alg_config = read_conf(args.config_alg)
    config = recursive_dict_update(config, alg_config)
    config = recursive_dict_update(config, env_config)

    # Training the net
    logger.info("Training the network")
    if th.cuda.is_available(): 
        config["device"] = "cuda"
        logger.info("CUDA device available")

    else:
        config["device"] = "cpu"
        logger.info("CUDA device not available. Using CPU")
    
    # Init runner so we can get env info
    logger.debug("Init runner")
    args = SN(**config)
    runner = r_REGISTRY[args.runner](args=args, logger=logger)

    # Set up schemes and groups here
    logger.debug("Init args data structure")
    env_info = runner.get_env_info()
    args.n_agents = env_info["n_agents"]
    args.n_actions = env_info["n_actions"]
    args.state_shape = env_info["state_shape"]

    # Default/Base scheme
    # TODO: Verify this is going to work with one group. I don't know what happens if we put two groups here
    logger.debug("Init buffer")
    scheme = {
        "state": {"vshape": env_info["state_shape"]},
        "obs": {"vshape": env_info["obs_shape"], "group": "agents"},
        "actions": {"vshape": (1,), "group": "agents", "dtype": th.int},
        "avail_actions": {"vshape": (env_info["n_actions"],), "group": "agents", "dtype": th.int},
        "reward": {"vshape": (1,)},
        "terminated": {"vshape": (1,), "dtype": th.uint8},
    }
    groups = {
        "agents": args.n_agents
    }
    preprocess = {
        "actions": ("actions_onehot", [OneHot(out_dim=args.n_actions)])
    }

    buffer = ReplayBuffer(scheme, groups, args.buffer_size, env_info["episode_limit"] + 1,
                          preprocess=preprocess,
                          device="cpu" if args.buffer_cpu_only else args.device)

    # Setup multiagent controller here
    logger.debug("Init Multi Agent Controller")
    mac = mac_REGISTRY[args.mac](buffer.scheme, groups, args)

    # Give runner the scheme
    logger.debug("Setup the runner")
    runner.setup(scheme=scheme, groups=groups, preprocess=preprocess, mac=mac)

    # Learner
    logger.debug("Init the learner")
    learner = le_REGISTRY[args.learner](mac, buffer.scheme, logger, args)
    
    # start training
    episode = 0
    last_test_T = -args.test_interval - 1
    last_log_T = 0
    model_save_time = 0

    start_time = time.time()
    last_time = start_time

    logger.info("Beginning training for {} timesteps".format(args.t_max))

    while runner.t_env <= args.t_max:

        # Run for a whole episode at a time
        episode_batch = runner.run(test_mode=False)
        buffer.insert_episode_batch(episode_batch)

        if buffer.can_sample(args.batch_size):
            episode_sample = buffer.sample(args.batch_size)

            # Truncate batch to only filled timesteps
            max_ep_t = episode_sample.max_t_filled()
            episode_sample = episode_sample[:, :max_ep_t]

            if episode_sample.device != args.device:
                episode_sample.to(args.device)

            learner.train(episode_sample, runner.t_env, episode)

        if args.save_model and (runner.t_env - model_save_time >= args.save_model_interval or model_save_time == 0):
            model_save_time = runner.t_env
            save_path = os.path.join(args.local_results_path, "models", args.unique_token, str(runner.t_env))
            #"results/models/{}".format(unique_token)
            os.makedirs(save_path, exist_ok=True)
            logger.info("Saving models to {}".format(save_path))

            # learner should handle saving/loading -- delegate actor save/load to mac,
            # use appropriate filenames to do critics, optimizer states
            learner.save_models(save_path)

        episode += args.batch_size_run

        if (runner.t_env - last_log_T) >= args.log_interval:
            logger.log_stat("episode", episode, runner.t_env)
            logger.print_recent_stats()
            last_log_T = runner.t_env


    # Test via Mesa
    logger.info("Initialize MESA")
